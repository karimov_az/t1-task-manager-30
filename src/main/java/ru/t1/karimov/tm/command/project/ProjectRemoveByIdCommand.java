package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        @Nullable final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        getProjectTaskService().removeProjectById(userId, id);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

}
