package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description) throws AbstractFieldException;

    @NotNull
    Project create(@NotNull String userId, @NotNull String name) throws AbstractFieldException;

}
