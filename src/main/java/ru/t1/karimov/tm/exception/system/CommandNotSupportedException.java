package ru.t1.karimov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command cannot be empty...");
    }

    public CommandNotSupportedException(@Nullable String command) {
        super("Error! Command \"" + command + "\" not supported...");
    }

}
